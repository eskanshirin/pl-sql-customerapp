CREATE OR REPLACE PACKAGE customerPackage AS
    PROCEDURE changePackage(vCustomer_num VARCHAR2, v_package VARCHAR2);
    PROCEDURE createRequest(vCustomer_num IN VARCHAR2, v_msg IN VARCHAR2, v_req_id IN VARCHAR2);
END customerPackage;

CREATE OR REPLACE PROCEDURE changePackage(vCustomer_num IN VARCHAR2, v_package IN VARCHAR2)
AS
BEGIN
    UPDATE customers
    SET package = v_package
    WHERE customer_num = vCustomer_num;
END;

CREATE OR REPLACE PROCEDURE createRequest(vcustomer_num IN VARCHAR2, 
    vnotes IN VARCHAR2, vinstallnum in VARCHAR2, vDate in Date )
    
AS

BEGIN
     INSERT INTO installation VALUES ( vinstallnum,vcustomer_num,'2632637163',VNOTES,vDate );
END;