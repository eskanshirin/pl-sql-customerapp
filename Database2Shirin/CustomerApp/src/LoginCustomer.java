import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.math.BigInteger;
import java.sql.*;
import java.util.Scanner;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class LoginCustomer {
	private static SecureRandom random = new SecureRandom();
	//Prompts the user to input their login info, returns true if they are a valid user, false otherwise
	  public String login(Connection con) throws SQLException{
		  Scanner sc = new Scanner(System.in);
		  System.out.print("Enter userName old user");
		  String userName =sc.nextLine();
		  System.out.print("Enter passWord old user");
		  String pass =sc.nextLine();
		  if(login(userName,pass,con)) {
			  return userName;
		  } else {
	        return null;
		  }
	  }
	  public boolean login(String username, String password, Connection con) throws SQLException {
			Statement statement = con.createStatement();

		    ResultSet resultSet = statement.executeQuery("select SALT, passhash from CUSTOMERS"
		    		+ " where CUSTOMER_NUM = '" + username + "'");
		    String salty="";
		    byte[] hashi=null;
		    boolean valid = true;
		    if (resultSet.next()) {
		    	salty = resultSet.getString("salt");
		    	hashi = resultSet.getBytes("passhash");
			    byte[] comparehash = hash(password, salty);
			    for (int i = 0; i < hashi.length; i++) {
			    	valid = valid && hashi[i] == comparehash[i];
			    }
		    } else {
		    	valid = false;
		    }
		    resultSet.close();
		    statement.close();
		    con.close();
		    return valid;
		}
	// Helper Functions below:
	// getConnection() - obtains a connection
	// getSalt() - creates a randomly generated string
	// hash() - takes a password and a salt as input and then computes their hash

	// Takes a password and a salt a performs a one way hashing on them, returning an array of bytes.
	  public byte[] hash(String password, String salt){
	    try {
	      SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
		
	/* When defining the keyspec, in addition to passing in the password and salt, we also pass in
				a number of iterations (1024) and a key size (256). The number of iterations, 1024, is the
				number of times we perform our hashing function on the input. Normally, you could increase security
				further by using a different number of iterations for each user (in the same way you use a different
				salt for each user) and storing that number of iterations. Here, we just use a constant number of
				iterations. The key size is the number of bits we want in the output hash
	  */
	      PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 1024, 256);

	      SecretKey key = skf.generateSecret(spec);
	      byte[] hash = key.getEncoded();
	      return hash;
	    } catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
	      throw new RuntimeException( e );
	    }
	  }
	}
