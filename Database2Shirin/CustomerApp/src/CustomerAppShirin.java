import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class CustomerAppShirin {
		static Scanner sc;
		static String url = "jdbc:oracle:thin:/@localhost:1521:XE";
		static Connection con;
		public static void main(String[] args) throws SQLException, ParseException {
		
	
       	Connection con = DriverManager.getConnection(url,"system","shirin5");
		// call the loginshirin class to comare salt and hashcode of user 
		LoginCustomer customer1 = new LoginCustomer();
		String user=customer1.login(con);
			if(user != null) {
        System.out.println(user);
			pleaseChoose(user,con);	
        } else {
        	System.out.println("Login failed");
        };
        con.close();
	}
		/*
		 * this method is all options that customer can choose to review or update or modify his package and 
		 * and everytime he did something we will show again another options to choose.
		 */
		public static void pleaseChoose(String user,Connection con) throws SQLException, ParseException {
	
		 sc = new Scanner(System.in);
		 System.out.print("enter the number of option that you want please\n");
		 System.out.println("1-Information about your current package and usage\n");
		 System.out.println("2- Modify package and eligibility\n ");
		 System.out.println("3- Schedule service request\n");
		 System.out.println("4-   invoice Status\n");
		// here with do while, preventing user to input string 
		 boolean end = false;
		 int choose =0;
			do
			{
			   try{
				   choose = sc.nextInt();
			       end = true;
			   }catch(InputMismatchException ex) {
			       System.out.println("An error ocurred please enter 1 or 2");
			       end = false;
			       System.out.println("Enter 1 if you agree and 2 if you are not ");
			       sc.nextLine();
			   }
			}while(end == false);
		
		  
		  if (choose == 1) {
				customerinfopackageusage(user); 
		  }
		
		  if ( choose == 2) {
			  	customerModifyPackage(user);
		  }  	
		  if ( choose == 3) {
		  scheduleService(user);
		  }
		  if ( choose == 4) {
		  invoiceStatus( user);
		  }
		
	}
		/*
		 * general information about the package already has, price usage plan
		 */
	     public static void customerinfopackageusage(String user) throws SQLException, ParseException {
	    	 Connection con = DriverManager.getConnection(url,"system","shirin5");
	    	 Statement statement = null;
       
	    	 statement = con.createStatement();
        	 ResultSet resultSet = statement.executeQuery( "SELECT package, description, features,"
        	 		+ " monthlycost FROM internetpackages "
    				+ " JOIN customers using(package)"
    				+ "WHERE customer_num  = '" + user + "'");
        	 if (resultSet.next()) {
        	 System.out.println("Your package type is " + resultSet.getString("package"));
        	 System.out.println("some Description" + resultSet.getString("description"));
        	 System.out.println("feature of your plan is " + resultSet.getString("features"));
        	 System.out.println("the price of your package is " + resultSet.getInt("monthlycost"));
        	 
        	 System.out.println("Choose between options again");
    
        	 pleaseChoose( user,con);
    		
        	 } else {
        		 System.out.println("Choose between options again");
	            	pleaseChoose( user,con);
        	 }

			 	resultSet.close();
			    statement.close();
			    con.close();		
	
        }
        /*
         * evaluate current package of customer and check his eligibility ,he can not get package cheaper than 
         * current package he has
         */
        
        public static void  customerModifyPackage(String user)  throws SQLException, ParseException {
        	Connection con = DriverManager.getConnection(url,"system","shirin5");
        	
        	System.out.println("please wait we are validating your request for eligibility");
        	Statement statement = con.createStatement();
        	
        	// customer current package price
        	int currentpackagePrice=0;
        	 ResultSet resultSet = statement.executeQuery( "SELECT package, monthlycost FROM internetpackages  "
      				+ " JOIN customers using(package)"
      				+ "WHERE customer_num  = '" + user + "'");
          	 if (resultSet.next()) {
          	 System.out.println("The price of your package  is " + resultSet.getInt("monthlycost"));
          	 currentpackagePrice = resultSet.getInt("monthlycost");
          	 }
          	System.out.println(currentpackagePrice);
        	
        	 String packageName;
        	 int packagePrice;
        	 ResultSet resultSet2 = statement.executeQuery( "SELECT package,monthlycost FROM internetpackages");
        	 while (resultSet2.next()) {
        		 packageName =  resultSet2.getString("package");
        		 packagePrice = resultSet2.getInt("monthlycost");
        		 if (currentpackagePrice < packagePrice){
        			 System.out.println("for the package of " + packageName +"the price is: " + packagePrice);
        			 System.out.println("you are eligible for this packge would you like to upgrade it for you?");
        			 System.out.println(packageName);
        			 System.out.println("Enter 1 if you agree and 2 if you are not ");
        			  
        			  boolean end = false;
        			  int answer=0;
        			

        			do
        			{
        			   try{
        				   answer = sc.nextInt();
        			       end = true;
        			   }catch(InputMismatchException ex) {
        			       System.out.println("An error ocurred please enter 1 or 2");
        			       end = false;
        			       System.out.println("Enter 1 if you agree and 2 if you are not ");
        			       sc.nextLine();
        			   }
        			}while(end == false);
        		
        		            if(answer == 1) {
        	        			  customerModifyPackage2(user , packageName);
        		            }
        		  
        		 }else {
        			 System.out.println("Choose between options again");
		            	pleaseChoose( user,con);
        		 }
        	          	 
        		 }
        	 
        		resultSet.close();
    		    statement.close();
    		    con.close();	
       
        }
        /*
         * this method calls inside previouse method, if customer eligible to modify the package 
         * he confirms and within this method we upgrade his package
         */
        	public static void  customerModifyPackage2(String user , String packageName)  throws SQLException {
	        	System.out.println(packageName + user);
	        	CallableStatement cstmt = null;
	        	String statement = null;
	        	Connection con = DriverManager.getConnection(url,"system","shirin5");
	        	try {
	        	con.setAutoCommit(false);
	        	
				 statement = "{call changePackage(?, ?)}";
				cstmt = con.prepareCall(statement);
				cstmt.setString(1, packageName);
				cstmt.setString(2, user);
				cstmt.executeQuery();
				con.commit();
				System.out.println("Package changed sucessfully!\n\n");
	        	} catch (SQLException e) {
	
	    		System.out.println(e.getMessage());
	    		con.rollback();

	        	} finally {
	
	    		if (statement != null) {
	    			cstmt.close();
    			}

    		}
	        	   con.close();
			
		}
        /*
         * if customer needs to set an appointment for services he gives the date he wants 
         * and small note that shows for which purpose he wants this service
         */
        	public static void scheduleService(String user) throws SQLException, ParseException {
        		Connection con = DriverManager.getConnection(url,"system","shirin5");
        		String statement = null;
        		CallableStatement cstmt  = null;
        		try {
        			con.setAutoCommit(false);
    		
	    		System.out.println("Please enter the servic you want?\n");
	    		
	    		String note = sc.next();
	    		
	    		System.out.println("Please enter the Date you want in this format \"yyyy-MM-dd\"?");
	    		String customerDate = sc.next();
	    		System.out.println("Please wait we are scheduling an appointment for you");

	    		Date pubDate = Date.valueOf(
    		        LocalDate.from(
    		          DateTimeFormatter.ofPattern(
    		           "yyyy-MM-dd"
    		        		
    		          ).parse(
    		        		  customerDate)));
         
	    		statement = "{call createRequest(?, ?, ?,?)}";
	    		cstmt = con.prepareCall(statement);
			
			int counter = 0;
			cstmt.setString(1, user);
			cstmt.setString(2, note);
			cstmt.setString(3, "23456789"+counter++);
			cstmt.setDate(4, pubDate);
			//counter++;
			cstmt.execute();
			con.commit();
			System.out.println("appointment set successfully for you");
			 System.out.println("Choose between options again");
	     	pleaseChoose( user,con);
	        } catch (SQLException e) {

    		System.out.println(e.getMessage());
    		System.out.println("please change the instalation number as it is a pk");
    		System.out.println("Choose between options again");
         	pleaseChoose( user,con);
    		con.rollback();

    		} catch (ParseException e){
    			System.out.println(e.getMessage());
    			
    		}
        	finally {

    		if (statement != null) {
    			cstmt.close();
    			}

    		}
        		   con.close();
        }
        /*
         * check if customer has any debt or not
         */
        	public static void invoiceStatus(String user) throws SQLException, ParseException {
        		Connection con = DriverManager.getConnection(url,"system","shirin5");
        		Statement statement = null;
        		try {
        			statement = con.createStatement();
        			ResultSet resultSet = statement.executeQuery( "SELECT billing_date, duedate, recieved FROM invoicing  "
    				
    					+ "WHERE customer_num  = '" + user + "'");
        			String billingDate = "";
        			String dudate ="";
        			String recieve = "";
        	
	        	 if (resultSet.next()) {
	        	 System.out.println("Your bill date is " + resultSet.getString("billing_date"));
	        	 System.out.println("duedate" + resultSet.getString("duedate"));
	        	 System.out.println("receive date is " + resultSet.getString("recieved"));
	        	
	        	 dudate =resultSet.getString("duedate");
	        	 recieve = resultSet.getString("recieved");
	        	 SimpleDateFormat textFormat = new SimpleDateFormat("yyyy-MM-dd");
	        	 String paramDateAsString = dudate.substring(0,10);
	        	 java.util.Date myDate = null;
	        	 myDate = textFormat.parse(paramDateAsString);
	        	 String paramDateAsString2 = recieve.substring(0,10);
	        	 java.util.Date myDate2 = null;
	        	 myDate2 = textFormat.parse(paramDateAsString2);
	        	if( myDate.compareTo(myDate2)>0)  {
	        	 System.out.println("you dont have debt");
	        	 System.out.println("Choose between options again");
	          	pleaseChoose( user,con);
	        	 }else {
	        		 System.out.println("you  have debt");
	        		 System.out.println("Choose between options again");
	              	pleaseChoose( user,con);
	        		 
	        	 } }else {
	    		 System.out.println("");
	        	 }
	
				 resultSet.close();
				 
				    con.close();		
		
	        }catch (ParseException e) {
	
	    		System.out.println(e.getMessage());
	    		
	
	    		} finally {
	
	    		if (statement != null) {
	    			statement.close();
	    			}
	
	    		}
	        }
}